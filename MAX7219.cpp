//
// Libary for the MAX7219 LED display.
//
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
// 

#include "Arduino.h"
#include "MAX7219.h"
#include <SPI.h>

//--------------------------------------------------------------------------

MAX7219::MAX7219() :
   m_pinChipSelect(0)
{

};

//--------------------------------------------------------------------------

void MAX7219::begin(int pinChipSelect)
{
   m_pinChipSelect = pinChipSelect;
   
   // Initialize chip select pin
   pinMode(m_pinChipSelect, OUTPUT);
   digitalWrite(m_pinChipSelect, HIGH);
   
   // Initialize SPI driver
   SPI.begin();
   
   // Initialize display
   displayOff();                       
   displayTestOff();                   
   setIntensity(intensityDefault);     
   setDecodeMode(decodeModeDefault);
   setScanLimit(scanLimitDefault);
   clear();
   
};

//--------------------------------------------------------------------------

void MAX7219::writeInteger(long value)
{ 
   const int  numberDecimals = 0;

   writeNumber(value, numberDecimals);
}

//--------------------------------------------------------------------------

void MAX7219::writeNumber(long value, int numberDecimals)
{ 
   if (inRange(value, minInteger, maxInteger))
   {
      const bool isNegative = (value < 0);
      
      // Normalize to positive value
      long processedValue = abs(value); 

      const int numberValueDigits     = log10(processedValue) + 1;
      const int numberDigitsToDisplay = max(numberValueDigits, numberDecimals + 1);
              
      for (int digitPos = 0; digitPos < numberDigits; ++digitPos)
      {
         byte adress     = (byte)(digitPos + 1);
         byte digitValue = characterBlank; 
             
         if (digitPos < numberDigitsToDisplay)
         {
            digitValue      = (byte)(processedValue % 10);        // e.g. 1234 % 10    = 4
            processedValue  = (processedValue - digitValue) / 10; // e.g.(1234 - 4)/10 = 123
            
            if (numberDecimals > 0 && digitPos == numberDecimals)
            {  
               // Add decimal point to this digit
               digitValue = digitValue | segmentDP;
            }   
         }
         else if (isNegative && digitPos == numberDigitsToDisplay)
         {
            digitValue = characterMinus;
         }
                  
         writeRegister(adress, digitValue);
      }
   }
   else // value out of range
   {
      writeErrorMessage(errorValueOutOfRange);
   }    
}

//--------------------------------------------------------------------------

void MAX7219::writeFloat(float value, int numberDecimals)
{
   const long normValue = (long)(value * pow(10, numberDecimals));
     
   writeNumber(normValue, numberDecimals);
}

//--------------------------------------------------------------------------

void MAX7219::clear()
{
  for (int i = 0; i < numberDigits; ++i)
  {
    byte adress = (byte)(i + 1); 
    writeRegister(adress, characterBlank);
  }
}

//--------------------------------------------------------------------------

void MAX7219::writeCharacter(int digitPosition, byte character)
{
   if (inRange(character, character0, characterBlank) && 
       inRange(digitPosition, 0, numberDigits - 1))
   {
      byte adress = (byte)(digitPosition + 1);
      writeRegister(adress, character);
   }
   else
   {
      writeErrorMessage(errorValueOutOfRange);
   }
}

//--------------------------------------------------------------------------

void MAX7219::writeRegister(byte adress, byte value)
{  
   digitalWrite(m_pinChipSelect, LOW); 

   SPI.beginTransaction(SPISettings(spiTransferRateHz, MSBFIRST, SPI_MODE0));
   SPI.transfer(adress);
   SPI.transfer(value);
   SPI.endTransaction();
  
   digitalWrite(m_pinChipSelect, HIGH);

}

//--------------------------------------------------------------------------

void MAX7219::displayOff()
{
   writeRegister(registerShutdown, 0x0);
}

//--------------------------------------------------------------------------

void MAX7219::displayOn()
{
   writeRegister(registerShutdown, 0x1);
}

//--------------------------------------------------------------------------

void MAX7219::setIntensity(byte intensity)
{
   byte intensityToWrite = constrain(intensity, intensityMin, intensityMax);
   writeRegister(registerIntensity, intensityToWrite);
}

//--------------------------------------------------------------------------

void MAX7219::setDecodeMode(byte decodeMode)
{
   writeRegister(registerDecodeMode, decodeMode);
}

//--------------------------------------------------------------------------

void MAX7219::setScanLimit(byte scanLimit)
{
   byte scanLimitToWrite = constrain(scanLimit, scanLimitMin, scanLimitMax);
   writeRegister(registerScanLimit, scanLimitToWrite);
}

//--------------------------------------------------------------------------

void MAX7219::displayTestOn()
{
   writeRegister(registerDisplayTest, 0x1);
}

//--------------------------------------------------------------------------

void MAX7219::displayTestOff()
{
   writeRegister(registerDisplayTest, 0x0);
}

//--------------------------------------------------------------------------

void MAX7219::writeErrorMessage(int errorId)
{
   writeInteger(errorId);
   
   byte adress = 4;
   writeRegister(adress, characterE);
}

//--------------------------------------------------------------------------


bool MAX7219::inRange(int value, int min, int max)
{
   return (value >= min && value <= max);
}

//--------------------------------------------------------------------------

bool MAX7219::inRange(long value, long min, long max)
{
   return (value >= min && value <= max);
}



