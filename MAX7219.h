//
// Libary for MAX7219 based LED display.
// Utilizes the SPI library for communication with MAX7219.
// Note that SPI utilizes PIN 10-13!
//
// 1 -  MISO |O|O| +VCC - 2
// 3 -   SCK |O|O| MOSI - 4
// 5 - reset |O|O| GND  - 6
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#ifndef MAX7219_H
#define MAX7219_H

#include "Arduino.h"

class MAX7219
{
   public:
   
      // Number of digits on the 7-segment display.
      const int numberDigits = 8;
   
      // Constructor. Call begin() before using any of the other methods!
      MAX7219();

      // Initalizes the display. Default values are set (see below).
      // The display will be off when this method returns.
      void begin(int pinChipSelect);
      
      // Turns the display on/off.
      // The display can be programmed when it's off.
      void displayOn();
      void displayOff();

      // Sets the intensity (brightness) of the LEDs.
      void setIntensity(byte intensity);
      
      const byte intensityMin     = 0x0;
      const byte intensityMax     = 0xF;
      const byte intensityDefault = 0x8;    
          
      // Sets how many digits are scanned (displayed).
      //  0: digit 0 is displayed
      //  1: digits 0, 1 are displayed
      //     ...
      //  7: digits 0, 1, 2, 3, 4, 5, 6, 7 are displayed
      void setScanLimit(byte scanLimit);
      
      const byte scanLimitMin     = 0;
      const byte scanLimitMax     = 7;
      const byte scanLimitDefault = scanLimitMax;
      
      // Clears the display (no segments are displayed).
      void clear();
      
      // Displays an integer on the display.
      void writeInteger(long integer);
      
      const long maxInteger = 99999999;
      const long minInteger = -9999999;
      
      // Displays a floating point number with fixed number decimals
      void writeFloat(float value, int numberDecimals); 
      
      // Enables/disables the test mode in which all segments are activated.
      void displayTestOn();
      void displayTestOff();
      
      // Writes an error message on the format "E <errorId>".
      // The 'E' will be in digit position 5.
      // Error IDs > 900 are reserved for internal errors.
      void writeErrorMessage(int errorId);
      
      const int errorValueOutOfRange  = 901;
               
      // Sets if a written character shall be decoded according to the
      // characters defined below, or if each bit
      // of the written character shall be used to activate segments
      // according to the segments defined below.
      void setDecodeMode(byte decodeMode);    
      
      const byte decodeNone        = 0b00000000;
      const byte decodeDigitPos0   = 0b00000001;
      const byte decodeDigitPos1   = 0b00000010;
      const byte decodeDigitPos2   = 0b00000100;
      const byte decodeDigitPos3   = 0b00001000;
      const byte decodeDigitPos4   = 0b00010000;
      const byte decodeDigitPos5   = 0b00100000;
      const byte decodeDigitPos6   = 0b01000000;
      const byte decodeDigitPos7   = 0b10000000;
      const byte decodeAll         = 0b11111111;
      const byte decodeModeDefault = decodeAll;
      
      // Writes one of the following characters/segments to given digit position.
		// The right-most character is position 0.
      void writeCharacter(int digitPosition, byte character);
      
      const int digitPositionMin = 0;
      const int digitPositionMax = numberDigits - 1;
      
      // Characters for decoding mode (decode mode bit for digit pos = 1).
      const byte character0      = 0x0;
      const byte character9      = 0x9;
      const byte characterMinus  = 0xA;
      const byte characterE      = 0xB;
      const byte characterH      = 0xC;
      const byte characterL      = 0xD;
      const byte characterP      = 0xE;
      const byte characterBlank  = 0xF;
      
      // Segment bits for no decoding (decode mode bit for digit pos = 0).
      const byte segmentDP = 0b10000000;
      const byte segmentA  = 0b01000000;
      const byte segmentB  = 0b00100000;
      const byte segmentC  = 0b00010000;
      const byte segmentD  = 0b00001000;
      const byte segmentE  = 0b00000100;
      const byte segmentF  = 0b00000010;
      const byte segmentG  = 0b00000001;
      
      /* Segement layout
            
            ---A---
            |     |
            F     B
            |     |
            ---G---
            |     |
            E     C
            |     |
            ---D---(DP)
      */
           
   private:
   
      void writeNumber(long value, int numberDecimals);
   
      void writeRegister(byte adress, byte value);
      
      bool inRange(int value, int max, int min);
      bool inRange(long value, long max, int long);
     
      int m_pinChipSelect;
           
      const int spiTransferRateHz = 10000000; // 10MHz

      // Register addresses
      const byte registerNoOp        = 0x0;
      const byte registerDigit0      = 0x1;
      const byte registerDigit7      = 0x8;
      const byte registerDecodeMode  = 0x9;
      const byte registerIntensity   = 0xA;
      const byte registerScanLimit   = 0xB;
      const byte registerShutdown    = 0xC;
      const byte registerDisplayTest = 0xF;
           
   
};

#endif // MAX7219_H
